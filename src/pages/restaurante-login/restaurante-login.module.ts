import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestauranteLoginPage } from './restaurante-login';

@NgModule({
  declarations: [
    RestauranteLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(RestauranteLoginPage),
  ],
})
export class RestauranteLoginPageModule {}
