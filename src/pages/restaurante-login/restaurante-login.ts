import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CadastroPage } from './../cadastro/cadastro';
import { PedidosPage } from './../pedidos/pedidos';
import { RestauranteCadastroPage } from './../../pages/restaurante-cadastro/restaurante-cadastro';
import { NgForm } from '@angular/forms';
import { User } from '../../providers/auth/user';
import { AuthService } from '../../providers/auth/auth-service';

/**
 * Generated class for the RestauranteLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurante-login',
  templateUrl: 'restaurante-login.html',
})
export class RestauranteLoginPage {

  user: User = new User();
  @ViewChild('form') form: NgForm;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Restaurante-login page');
  }

  Login(){
    if (this.form.form.valid){
      this.authService.signIn(this.user)
        .then(() => {
          this.navCtrl.setRoot(PedidosPage);
        })
        .catch((error: any) => {
          let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
          if (error.code == 'auth/invalid-email') {
            toast.setMessage('O e-mail digitado não é valido.');
          } else if (error.code == 'auth/user-disabled') {
            toast.setMessage('O usuário está desativado.');
          } else if (error.code == 'auth/user-not-found') {
            toast.setMessage('O usuário não foi encontrado.');
          } else if (error.code == 'auth/wrong-password') {
            toast.setMessage('A senha digitada não é valida.');
          }
          toast.present();
        });
    }
  }  

  GoToPagRegistro(){
    this.navCtrl.push(CadastroPage);
  } 

  newRestaurante(){
  	this.navCtrl.push('RestauranteCadastroPage');
  }
}