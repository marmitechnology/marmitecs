import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestauranteCadastroPage } from './restaurante-cadastro';

@NgModule({
  declarations: [
    RestauranteCadastroPage,
  ],
  imports: [
    IonicPageModule.forChild(RestauranteCadastroPage),
  ],
})
export class RestauranteCadastroPageModule {}
