import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RestauranteProvider } from './../../providers/restaurante/restaurante';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
 * Generated class for the RestauranteCadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurante-cadastro',
  templateUrl: 'restaurante-cadastro.html',
})
export class RestauranteCadastroPage {
	title: string;
	form: FormGroup;
	restaurante: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private provider: RestauranteProvider, private toast: ToastController) {
  	this.restaurante = this.navParams.data.restaurante || {};
  	this.createForm();
  	this.setupPageTitle();
  }

  private setupPageTitle(){
  	this.title = this.navParams.data.restaurante ? 'Alterando restaurante' : 'Novo Restaurante';
  }

  createForm(){
  	this.form = this.formBuilder.group({
  		key: [this.restaurante.key],
  		name: [this.restaurante.name, Validators.required],
      descricao: [this.restaurante.descricao, Validators.required],
  		cnpj: [this.restaurante.cnpj, Validators.required],
  		ddd: [this.restaurante.ddd, Validators.required],
  		endereco: [this.restaurante.endereco, Validators.required],
  		forma_de_pagamento: [this.restaurante.forma_de_pagamento, Validators.required],
  		horario: [this.restaurante.horario, Validators.required],
  		telefone: [this.restaurante.telefone, Validators.required],
  	});
  }
  onSubmit(){
  	if(this.form.valid){
  		this.provider.save(this.form.value)
  			.then(() => {
  				this.toast.create({ message: 'Restaurante salvo com sucesso.', duration: 3000}).present();
  			})
  			.catch((e) => {
  				this.toast.create({ message: 'Erro ao salvar o restaurante.', duration: 3000}).present();
  				console.log(e);
  			});
  	}
  }
}
