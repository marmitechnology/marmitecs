import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestauranteProvider } from './../../providers/restaurante/restaurante';
import { Observable } from 'rxjs/Observable';
import { PedidoRestaurantePage } from './../pedido-restaurante/pedido-restaurante';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  restaurantes: Observable<any>;
  
  constructor(public navCtrl: NavController, private provider: RestauranteProvider) {

  	this.restaurantes = this.provider.getAll();
  }

  goToPedidoRestaurante(restaurante: any){
  	//this.navCtrl.push('PedidoRestaurantePage', {restaurante: restaurante});

  	//maneira 2
  	this.navCtrl.push('PedidoRestaurantePage', {key: restaurante.key});
  	console.log(restaurante.key);
  }

}