import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PedidoProvider } from './../../providers/pedido/pedido';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
})
export class PedidosPage {
  pedidos: Observable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private provider: PedidoProvider, private afAuth:AngularFireAuth) {

  	this.pedidos = this.provider.getAll();
  	console.log(this.pedidos);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidosPage');
  }

}
