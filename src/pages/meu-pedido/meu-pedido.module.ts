import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeuPedidoPage } from './meu-pedido';

@NgModule({
  declarations: [
    MeuPedidoPage,
  ],
  imports: [
    IonicPageModule.forChild(MeuPedidoPage),
  ],
})
export class MeuPedidoPageModule {}
