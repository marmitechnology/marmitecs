import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComercianteCadastroPage } from './comerciante-cadastro';

@NgModule({
  declarations: [
    ComercianteCadastroPage,
  ],
  imports: [
    IonicPageModule.forChild(ComercianteCadastroPage),
  ],
})
export class ComercianteCadastroPageModule {}
