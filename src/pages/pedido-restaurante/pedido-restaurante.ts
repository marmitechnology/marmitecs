import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RestauranteProvider } from './../../providers/restaurante/restaurante';
import { PedidoProvider } from './../../providers/pedido/pedido';
import { DetalhePedidoPage } from './../../pages/detalhe-pedido/detalhe-pedido';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable } from 'rxjs/Observable';

//import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the PedidoRestaurantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedido-restaurante',
  templateUrl: 'pedido-restaurante.html',
})
export class PedidoRestaurantePage {
	//title: string;
	restaurantes: Observable<any>;
	restaurante: any;

	@ViewChild('form') form: NgForm;	

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	private provider: RestauranteProvider, 
  	private providerPedido: PedidoProvider, 
  	private formBuilder: FormBuilder, private toast: ToastController) {
  	//this.restaurante = this.navParams.data.restaurante || {};
  	//maneira 2
  	this.restaurante = {};
  	//this.createForm();

  	
  	if (this.navParams.data.key){
  		const subscribe = this.provider.get(this.navParams.data.key)
  		  .subscribe((c: any) => {
  		  	subscribe.unsubscribe();

  		  	this.restaurante = c;
  		  })
  	}
  	
  }
  
  /*
  createForm(){
  	this.form = this.formBuilder.group({
  		key: [this.pedido.key],
  		name_prato: [this.pedido.name_prato],
        descricao_prato: [this.pedido.descricao_prato],
  		preco_prato: [this.pedido.preco_prato]
  	});
  } */

  registrarPedido(key, feijoada, frango, carne, coca300, fanta300, coca2){
  	  this.providerPedido.registrarPedido(key, feijoada, frango, carne, coca300, fanta300, coca2);	  	
  }
}
