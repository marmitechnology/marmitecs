import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoRestaurantePage } from './pedido-restaurante';

@NgModule({
  declarations: [
    PedidoRestaurantePage,
  ],
  imports: [
    IonicPageModule.forChild(PedidoRestaurantePage),
  ],
})
export class PedidoRestaurantePageModule {}
