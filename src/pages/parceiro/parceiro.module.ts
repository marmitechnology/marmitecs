import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParceiroPage } from './parceiro';

@NgModule({
  declarations: [
    ParceiroPage,
  ],
  imports: [
    IonicPageModule.forChild(ParceiroPage),
  ],
})
export class ParceiroPageModule {}
