import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class RestauranteProvider {
	private PATH = 'comerciante/restaurante/';
    user: Observable<any>;
    userId : string;

    
  constructor(public db: AngularFireDatabase, private afAuth:AngularFireAuth) {
    afAuth.authState.subscribe(user => {
       if (user) { this.userId = user.uid }
      return user.uid;
    });
  }

    getAll(){
        
    	return this.db.list(this.PATH, ref => ref.orderByChild('name'))
	    	.snapshotChanges()
	    	.map(changes => {
	    		return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
	    	})
    }

    get(key: string){
    	return this.db.object(this.PATH + key)
    		.snapshotChanges()
    		.map(c =>{
    			return {key: c.key, ...c.payload.val() };
    		});
    }

    save(restaurante: any){
    	return new Promise((resolve, reject) =>{
    		if(this.userId){
    			this.db.list(this.PATH)
    			.update(this.userId, {name: restaurante.name, cnpj: restaurante.cnpj, ddd: restaurante.ddd, endereco: restaurante.endereco, forma_de_pagamento: restaurante.forma_de_pagamento, horario: restaurante.horario, telefone: restaurante.telefone, descricao: restaurante.descricao})
    			.then(() => resolve())
    			.catch((e) => reject(e));
    		}else{
    			this.db.list(this.PATH)
    			.push({name: restaurante.name, cnpj: restaurante.cnpj, ddd: restaurante.ddd, endereco: restaurante.endereco, forma_de_pagamento: restaurante.forma_de_pagamento, horario: restaurante.horario, telefone: restaurante.telefone, descricao: restaurante.descricao})
    			.then(() => resolve());
    		}
    	})
        
    }

    remove(key: string){
    	return this.db.list(this.PATH).remove(key);
    }  
    
    ionViewDidLoad(){
      console.log(this.getCurrentUser());
    }

    getCurrentUser(): any {
        this.afAuth.authState.subscribe(data => {
          console.log(data.uid);
          return data.uid;
        });
    }  

}
