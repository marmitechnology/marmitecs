import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { ToastController } from 'ionic-angular';


@Injectable()
export class PedidoProvider {
	/*
	key: string;
	private PATH = 'comerciante/restaurante/'+ this.key + '/pedido/';
	*/
  userId : string;
  user: Observable<any>;
  constructor(public db: AngularFireDatabase, private afAuth:AngularFireAuth, private toastCtrl: ToastController) {}

	registrarPedido(key: any, feijoada: any, frango: any, carne: any, coca300: any, fanta300: any, coca2: any) {	
		console.log(key);
        if (feijoada == false) {
            feijoada= '';
        }else{
            feijoada = 'feijoada';
        }
        if (frango == false) {
            frango= '';
        }else{
            frango = 'frango';
        }
        if (carne == false) {
            carne= '';
        }else{
            carne = 'carne';
        }
        if (coca300 == false) {
            coca300 = '';
        }else{
            coca300 ='Coca-cola 300ML';
        }
        if (fanta300 == false) {
            fanta300= '';
        }else{
            fanta300 = 'Fanta 300ML';
        }
        if (coca2 == false) {
            coca2= '';
        }else{
            coca2='Coca-cola 2 litros';
        }
		return new Promise((resolve, reject) =>{
    		if(key){
    		this.db.list('comerciante/restaurante/'+ key + '/pedido/')
    		.push( { feijoada: feijoada, frango: frango,carne: carne, coca300: coca300, fanta300: fanta300,coca2: coca2 })
    			.then(() => {
                    resolve();
                    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
                    toast.setMessage('Pedido confirmado com sucesso.');
                    toast.present();                    
                });
    		}else{
    			this.db.list('comerciante/restaurante/pedidos_indefinidos/pedido/')
    			.push({ feijoada: feijoada, frango: frango,carne: carne, coca300: coca300, fanta300: fanta300,coca2: coca2 })
    			.then(() => resolve());
    		}
    	})
    }		
    
    getAll(){
        this.afAuth.authState.subscribe(user => {
           if (user) { this.userId = user.uid }
        });        
        console.log(this.userId);
    	return this.db.list('comerciante/restaurante/'+ this.userId + '/pedido/', ref => ref.orderByChild('name'))
	    	.snapshotChanges()
	    	.map(changes => {
	    		return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
	    	})
    }
    /*
    get(key: string){
    	return this.db.object(this.PATH + key)
    		.snapshotChanges()
    		.map(c =>{
    			return {key: c.key, ...c.payload.val() };
    		});
    }

    save(pedido: any){
    	return new Promise((resolve, reject) =>{
    		if(pedido.key){
    			this.db.list(this.PATH)
    			.update(pedido.key, {name: pedido.name, descricao: pedido.descricao, preco: pedido.preco})
    			.then(() => resolve())
    			.catch((e) => reject(e));
    		}else{
    			this.db.list(this.PATH)
    			.push({name: pedido.name, descricao: pedido.descricao, preco: pedido.preco})
    			.then(() => resolve());
    		}
    	})
    }

    remove(key: string){
    	return this.db.list(this.PATH).remove(key);
    }  
	*/
}