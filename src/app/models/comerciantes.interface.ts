export interface Comerciante{
	cnpj: number;
	cpf: number;
	ddd: number;
	email: string;
	[nome: number]:{primeiro: string; sobrenome: string};
	telefone: string;
	orgao_emissor: string;
	rg: string;
	Restaurantes:{
		[idRestaurante: string]: Restaurante		
	}
}

interface Restaurante{
	cnpj: number;
	ddd: number;
	endereco: string;
	[forma_de_pagamento: number]:{forma_de_pagamento: string; sobrenome: string};
	horario_de_funcionamento: string;
	nome: string;
	telefone: number;
	Pratos: {
		[idPrato: number]: Prato
	}
}

interface Prato{
	descricao_prato: string;
	nome_prato: string;
	preco_prato: number;
	tipo_prato: string;
}