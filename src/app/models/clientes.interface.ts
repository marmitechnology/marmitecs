export interface Cliente {
	idCliente: string;
	cpf: number;
	cnpj: number;
	email: string;
	endereco: string;
	[nome: number]:{primeiro: string; sobrenome: string};
	telefone: string;
	Avaliacao:{
		[idAvaliacao: string]: Avaliacao
	}
}

export interface Avaliacao{
	idPedido: string;
	avaliacao_data: string;
	avaliacao_desc: string;
	avaliacao_nota: number;
}