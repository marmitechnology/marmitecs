import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RestauranteLoginPage } from '../pages/restaurante-login/restaurante-login';
import { CadastroPage } from '../pages/cadastro/cadastro';
//import { PedidoRestaurantePage } from '../pages/pedido-restaurante/pedido-restaurante';

//Modulos para aparecerem no menu lateral
import { LoginPageModule } from '../pages/login/login.module';
import { CarteiraPageModule } from '../pages/carteira/carteira.module';
import { FavoritosPageModule } from '../pages/favoritos/favoritos.module';
import { PedidosPageModule } from '../pages/pedidos/pedidos.module';
import { AvisosPageModule } from '../pages/avisos/avisos.module';
import { CuponsPageModule } from '../pages/cupons/cupons.module';
import { FormasPagamentoPageModule } from '../pages/formas-pagamento/formas-pagamento.module';
import { ConfigPageModule } from '../pages/config/config.module';
import { SugerirPageModule } from '../pages/sugerir/sugerir.module';
import { AjudaPageModule } from '../pages/ajuda/ajuda.module';
import { ParceiroPageModule } from '../pages/parceiro/parceiro.module';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//geolocation
//import { Geolocation  } from '@ionic-native/geolocation';

import { AuthService } from '../providers/auth/auth-service';

import { FIREBASE_CONFIG } from './app.firebase.config';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { ComercianteProvider } from '../providers/comerciante/comerciante';
import { RestauranteProvider } from '../providers/restaurante/restaurante';
import { PedidoProvider } from '../providers/pedido/pedido';

//import { GooglePlus } from '@ionic-native/google-plus';
//import { Facebook } from '@ionic-native/facebook';
//import { TwitterConnect } from '@ionic-native/twitter-connect';
//import { ImagePicker } from '@ionic-native/image-picker';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RestauranteLoginPage,
    CadastroPage,
    //PedidoRestaurantePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule, 
    PedidosPageModule,
    LoginPageModule,
    ParceiroPageModule,
    CarteiraPageModule,
    FavoritosPageModule,
    AvisosPageModule,
    CuponsPageModule,
    FormasPagamentoPageModule,
    ConfigPageModule,
    SugerirPageModule,
    AjudaPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RestauranteLoginPage,
    CadastroPage,
    //PedidoRestaurantePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    RestauranteProvider,
    PedidoProvider
    //ComercianteProvider,
    //Geolocation,  
  ]
})
export class AppModule {}
