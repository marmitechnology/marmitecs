import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from 'angularfire2/auth';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CarteiraPage } from '../pages/carteira/carteira';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { PedidosPage } from '../pages/pedidos/pedidos';
import { AvisosPage } from '../pages/avisos/avisos';
import { CuponsPage } from '../pages/cupons/cupons';
import { FormasPagamentoPage } from '../pages/formas-pagamento/formas-pagamento';
import { ConfigPage } from '../pages/config/config';
import { SugerirPage } from '../pages/sugerir/sugerir';
import { AjudaPage } from '../pages/ajuda/ajuda';
import { ParceiroPage } from '../pages/parceiro/parceiro';
import { RestauranteCadastroPage } from '../pages/restaurante-cadastro/restaurante-cadastro';
//import { RestauranteLoginPage } from '../pages/restaurante-login/restaurante-login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, afAuth: AngularFireAuth) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Entrar ou cadastrar', component: LoginPage },
      { title: 'Carteira', component: CarteiraPage },
      { title: 'Favoritos', component: FavoritosPage },
      { title: 'Pedidos', component: PedidosPage },
      { title: 'Avisos', component: AvisosPage },
      { title: 'Cupons', component: CuponsPage },
      { title: 'Formas de pagamento', component: FormasPagamentoPage },
      { title: 'Configurações', component: ConfigPage },
      { title: 'Sugerir restaurante', component: SugerirPage },
      { title: 'Ajuda', component: AjudaPage },
      { title: 'Seja um parceiro', component: ParceiroPage },
      { title: 'Adicione um restaurante', component: RestauranteCadastroPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
